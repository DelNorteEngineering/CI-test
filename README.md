# CI Test

Contains a simple python script and yaml config file for testing it.
Auto-generated docs are here: https://delnorteengineering.gitlab.io/CI-test

[![pipeline status](https://gitlab.com/DelNorteEngineering/CI-test/badges/master/pipeline.svg)](https://gitlab.com/DelNorteEngineering/CI-test/pipelines)
[![coverage report](https://gitlab.com/DelNorteEngineering/CI-test/badges/master/coverage.svg)](https://gitlab.com/DelNorteEngineering/CI-test/pipelines)
