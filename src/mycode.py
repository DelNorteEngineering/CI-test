
def add(a: int, b: int) -> int:
    """
    Adds two integers
    :rtype: int
    :param a: an integer to add
    :param b: the other integer to add
    :return: the sum of a and b
    """
    if isinstance(a, int) and isinstance(b, int):
        return a + b
    else:
        raise TypeError('I only like ints!')


def main():
    print('Hello World!')
    a = 2
    b = 2
    print('{} plus {} is {}'.format(a, b, add(a, b)))


if __name__ == '__main__':
    main()
