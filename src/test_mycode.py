import unittest
from mycode import *


class TestMyCode(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(2, 2), 4)
        self.assertEqual(add(0, 0), 0)
        self.assertEqual(add(-1, 1), 0)

        # Make sure we raise an exception on invalid types
        with self.assertRaises(TypeError):
            add('2', 2)

    def test_main(self):
        main()
        # self.failIf(False)
