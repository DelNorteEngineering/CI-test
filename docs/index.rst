.. src documentation master file, created by
   sphinx-quickstart on Tue Jul 10 01:23:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CI Test's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   mycode
   test_mycode


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
